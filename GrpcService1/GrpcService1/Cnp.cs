﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;

namespace GrpcService1
{
    public class Cnp
    {
        private string rawCnp;

        public void setRawCnp(string newCnp)
        {
            rawCnp = newCnp;
        }

        public string getGender()
        {
            if(getGenderDigit()%2==0)
            {
                return "Sex Feminin";
            }
            else
            {
                return "Sex Masculin";
            }
        }

        public string getYear()
        {
            string year = "";
            switch (getGenderDigit())
            {
                case '1':
                case '2':
                    year = "19";
                    break;
                case '3':
                case '4':
                    year = "18";
                    break;
                case '5':
                case '6':
                    year = "20";
                    break;
                default:
                    year = "-";
                    break;
            }
            year = year + rawCnp.Substring(1, 2);
            return year;
        }

        public string getMonth()
        {
            return rawCnp.Substring(3, 2);
        }

        public string getDay()
        {
            return rawCnp.Substring(5, 2);
        }

        public char getGenderDigit()
        {
            return rawCnp[0];
        }

        public string getAge()
        {
            string strdate = getDay() + "/" + getMonth() + "/" + getYear();
            var date = DateTime.Parse(strdate, new CultureInfo("en-GB", true));
            var currentdate = DateTime.Now;
            TimeSpan span = currentdate.Subtract(date);
            return Math.Round((Int32.Parse(span.Days.ToString()) / 365.25) - 1).ToString();
        }

        public string getCity()
        {
            return rawCnp.Substring(7, 2);
        }

        public bool isValid()
        {
            if (rawCnp.Length != 13)
            {
                return false;
            }
            foreach (char c in rawCnp)
            {
                if (c < '0' || c > '9')
                {
                    return false;
                }
            }
            if (getGenderDigit() == '0' || getGenderDigit() == '9')
            {
                return false;
            }
            if (Int32.Parse(getMonth()) < 1 || Int32.Parse(getMonth()) > 12)
            {
                return false;
            }
            if (DateTime.DaysInMonth(Int32.Parse(getYear()), Int32.Parse(getMonth())) < Int32.Parse(getDay()) || Int32.Parse(getDay()) < 0)
            {
                return false;
            }
            if (Int32.Parse(getCity()) < 1 || Int32.Parse(getCity()) > 52)
            {
                return false;
            }
            if (controlDigit() != (rawCnp[12]-'0'))
            {
                return false;
            }
            return true;
        }
        public int controlDigit()
        {
            int sum = 0;
            const string key = "279146358279";
            for (int index = 0; index < 12; index++)
            {
                sum += (rawCnp[index] - '0') * (key[index] - '0');
            }
            sum %= 11;
            if (sum == 10)
                return 1;
            else
            {
                return sum;
            }
        }
    }
}
