using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Grpc.Core;
using Microsoft.Extensions.Logging;
using System.Globalization;
namespace GrpcService1
{
    public class UserService : UserResponse.UserResponseBase
    {
        private readonly ILogger<UserService> _logger;
        public UserService(ILogger<UserService> logger)
        {
            _logger = logger;
        }

        public override Task<UserReply> SayToUser(UserRequest request, ServerCallContext context)
        {
            Cnp cnp = new Cnp();
            cnp.setRawCnp(request.User.Cnp);
            _logger.LogInformation("Nume: "+request.User.Name);
            if (cnp.isValid())
            {
                _logger.LogInformation(cnp.getGender());
                if(cnp.getGenderDigit()=='8' || cnp.getGenderDigit() == '7')
                {
                    return Task.FromResult(new UserReply
                    {
                        Message = "Hello, " + request.User.Name + ", CNP-ul tau este valid si esti o persoana rezidenta!"
                    });
                }
                _logger.LogInformation("Varsta: " + cnp.getAge());
                return Task.FromResult(new UserReply
                {
                    Message = "Hello, " + request.User.Name + ", CNP-ul tau este valid!"
                });
            }
            else
            {
                return Task.FromResult(new UserReply
                {
                    Message = "Hello, " + request.User.Name + ", CNP-ul tau nu este valid!"
                });;
            }    

        }
    }
}
