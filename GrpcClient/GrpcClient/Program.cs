﻿using System;
using System.Threading.Tasks;
using Grpc.Net.Client;
using GrpcService1;

namespace GrpcClient
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Console.WriteLine("Hello!");
            using var channel = GrpcChannel.ForAddress("https://localhost:5001");
            var client = new UserResponse.UserResponseClient(channel);
            
            string name;
            Console.WriteLine("Nume: ");
            name = Console.ReadLine();
            
            string cnp;
            Console.WriteLine("Cnp: ");
            cnp = Console.ReadLine();

            User user = new User() { Name = name, Cnp = cnp };

            var reply = await client.SayToUserAsync(new UserRequest { User = user });
            Console.WriteLine($"Greetings: {reply.Message} ");
            Console.WriteLine("Press any key to exit...");
            Console.ReadLine();
        }
    }
}
